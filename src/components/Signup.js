import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';

// components
import Loading from './Loading';

const Signup = ({ firebase }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [buyBudget, setBuyBudget] = useState(null);
  const [rentBudget, setRentBudget] = useState(null);
  const [district, setDistrict] = useState(null);
  const [estate, setEstate] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const [redirect, setRedirect] = useState(false);

  const _onClickSignup = () => {
    console.log('_onClickSignup', email, password);
    setIsLoading(true);
    return firebase
      .auth()
      .createUserWithEmailAndPassword(email, password)
      .then(auth => {
        console.log('auth', auth, auth.user._uid);
        setIsLoading(false);
        setRedirect(true);
        const body = {
          id: auth.user.uid,
          preferred_district: district,
          preferred_estate: estate,
          first_name: firstName,
          last_name: lastName,
          email: email,
          budget_for_buying: buyBudget,
          budget_for_rental: rentBudget,
        }
        console.log('body', body);
        fetch(`http://localhost:5000/add_user`, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          body: JSON.stringify(body),
        })
          .then(res => console.log(res))
          .catch(err => console.log(err));
      })
      .catch(function(error) {
        // Handle Errors here.
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log(errorCode, errorMessage);
      });
  }

  const onClickFilter = (type, value) => {
    console.log('onClickFilter', type, value);
    switch (type) {
      case 'district':
        return setDistrict(value);
      case 'buy':
        return setBuyBudget(value);
      case 'rent':
        return setRentBudget(value);
      default:
        return setEstate(value);
    }
  }

  if (isLoading) {
    return <Loading />
  }

  if (redirect) {
    return <Redirect to="/" />
  }

  return (
    <div className="d-flex justify-content-center login-container">
      <div className="form-signin login-form p-4 w-50">
        <img className="mb-4" src="/docs/4.3/assets/brand/bootstrap-solid.svg" alt="" width="72" height="72" />
        <h1 className="h1 mb-3 font-weight-normal">Customer Sign Up</h1>
        <label htmlFor="inputEmail" className="sr-only">Email address</label>
        <input value={email} onChange={e => setEmail(e.target.value)} type="email" id="inputEmail" className="form-control" placeholder="Email address" required="" autoFocus="" />
        <label htmlFor="firstName" className="sr-only">First Name</label>
        <input value={firstName} onChange={e => setFirstName(e.target.value)} type="text" id="firstName" className="form-control" placeholder="First Name" required="" autoFocus="" />
        <label htmlFor="lastName" className="sr-only">Last Name</label>
        <input value={lastName} onChange={e => setLastName(e.target.value)} type="text" id="lastName" className="form-control" placeholder="Last Name" required="" autoFocus="" />
        <label htmlFor="inputPassword" className="sr-only">Password</label>
        <input value={password} onChange={e => setPassword(e.target.value)} type="password" id="inputPassword" className="form-control" placeholder="Password" required="" />
        <CustomerInput
            buyBudget={buyBudget}
            rentBudget={rentBudget}
            district={district}
            estate={estate}
            onClickFilter={onClickFilter}
          />
        <div className="checkbox mb-3">
          <label>
            <input type="checkbox" value="remember-me" /> Remember me
          </label>
        </div>
        <button onClick={_onClickSignup} className="btn btn-lg btn-primary btn-block">Sign Up</button>
        <p className="mt-5 mb-3 text-muted">© 2019</p>
      </div>
    </div>
  )
}

const CustomerInput = ({ buyBudget, rentBudget, district, estate, onClickFilter }) => {
  const districtArray = ['Eastern', 'Southern', 'Central and Western', 'Sham Shui Po', 'Kowloon City', 'Wong Tai Sin', 'Yau Tsim Mong', 'North', 'Kwun Tong'];
  const estateArray = ['Kornhill', 'Greenwood Terrace (Cw)', 'Taikoo Shing', 'Grand Promenade', 'Lei King Wan', 'Amoy Gardens', 'Laguna City', 'Telford Gardens', 'Sceneway Garden', 'Tak Bo Garden'];
  const buyBudgetArray = [{ label: '$1,000,000', value: 1000000 }, { label: '$2,500,000', value: 2500000 }, { label: '$5,000,000', value: 5000000 }, { label: '$10,000,000', value: 10000000 }, { label: '$15,000,000', value: 15000000 }, { label: '$25,000,000', value: 25000000 }, { label: '$50,000,000', value: 50000000 }, { label: '$100,000,000', value: 100000000 },];
  const rentBudgetArray = [{ label: '$1,000', value: 1000 }, { label: '$5,000', value: 5000 }, { label: '$10,000', value: 10000 }, { label: '$15,000', value: 15000 }, { label: '$20,00', value: 20000 }, { label: '$30,00', value: 30000 }, { label: '$50,000', value: 50000 }];

  return (
    <div className="d-flex mt-3 mb-2">
      <div className="dropdown">
        <button className="btn btn-secondary dropdown-toggle" type="button" id="district" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {district ? district : 'District'}
        </button>
        <div className="dropdown-menu" aria-labelledby="district">
          {
            districtArray.map((d, i) => <button key={i} onClick={() => onClickFilter('district', d)} className="dropdown-item">{d}</button>)
          }
        </div>
      </div>
      <div className="dropdown">
        <button className="btn btn-secondary dropdown-toggle ml-2" type="button" id="estate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {estate ? estate : 'Estate'}
        </button>
        <div className="dropdown-menu" aria-labelledby="estate">
          {
            estateArray.map((e, i) => <button key={i} onClick={() => onClickFilter('estate', e)} className="dropdown-item">{e}</button>)
          }
        </div>
      </div>
      <div className="dropdown">
        <button className="btn btn-secondary dropdown-toggle ml-2" type="button" id="budget" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {buyBudget ? buyBudget : 'Budget for Buy'}
        </button>
        <div className="dropdown-menu" aria-labelledby="budget">
          {
            buyBudgetArray.map((b, i) => <button key={i} onClick={() => onClickFilter('buy', b.value)} className="dropdown-item">{b.label}</button>)
          }
        </div>
      </div>
      <div className="dropdown">
        <button className="btn btn-secondary dropdown-toggle ml-2" type="button" id="budget" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {rentBudget ? rentBudget : 'Budget for Rent'}
        </button>
        <div className="dropdown-menu" aria-labelledby="budget">
          {
            rentBudgetArray.map((b, i) => <button key={i} onClick={() => onClickFilter('rent', b.value)} className="dropdown-item">{b.label}</button>)
          }
        </div>
      </div>
    </div>
  )
}


export default Signup;
