import React from 'react';

const Footer = () => {
  return (
    <footer className="footer mt-auto py-3">
      <div className="container">
        <span className="text-muted">K's Property Site Prowered by K Chau © 2019.</span>
      </div>
    </footer>
  )
}

export default Footer;
