import React, { useState, useEffect } from 'react';
import { Redirect } from 'react-router-dom';

const SearchBox = ({ onClickSearch }) => {
  const [budgetMinArray, setBudgetMinArray] = useState([]);
  const [budgetMaxArray, setBudgetMaxArray] = useState([]);
  const [budgetMin, setBudgetMin] = useState('Budget (Min)');
  const [budgetMax, setBudgetMax] = useState('Budget (Max)');
  const [district, setDistrict] = useState('District');
  const [estate, setEstate] = useState('Estate');
  const [search, setSearch] = useState(undefined);
  const [type, setType] = useState('buy');
  const buyBudgetMin = ['$1,000,000', '$2,500,000', '$5,000,000', '$10,000,000', '$15,000,000', '$25,000,000', '$50,000,000', '$100,000,000'];
  const buyBudgetMax = ['$1,000,000', '$2,500,000', '$5,000,000', '$10,000,000', '$15,000,000', '$25,000,000', '$50,000,000', '$100,000,000'];
  const rentBudgetMin = ['$1,000', '$5,000', '$10,000', '$15,000', '$20,00', '$30,00', '$50,000'];
  const rentBudgetMax = ['$1,000', '$5,000', '$10,000', '$15,000', '$20,00', '$30,00', '$50,000'];

  useEffect(() => {
    setBudgetMinArray(buyBudgetMin);
    setBudgetMaxArray(buyBudgetMax);
  }, [])

  const _onClickSearch = () => {
    // console.log('_onClickSearch', estate, district, type, budgetMin, budgetMax);
    onClickSearch(estate, district, type, budgetMin, budgetMax);
  }

  const _onChangeSearch = e => {
    // console.log('_onChangeSearch', value);
    setSearch(e.target.value);
  }

  const _onClickChangeType = type => {
    // console.log('_onClickChangeType', type);
    setType(type);
    if (type === 'buy') {
      setBudgetMinArray(buyBudgetMin);
      setBudgetMaxArray(buyBudgetMax);
      setBudgetMin('Budget (Min)');
      setBudgetMax('Budget (Max)');
      return;
    }
    setBudgetMinArray(rentBudgetMin);
    setBudgetMaxArray(rentBudgetMax);
    setBudgetMin('Budget (Min)');
    setBudgetMax('Budget (Max)');
  }

  const onClickFilter = (type, value) => {
    // console.log('onClickFilter', type, value);
    switch (type) {
      case 'district':
        setDistrict(value);
        break;
      case 'budgetMin':
        setBudgetMin(value);
        break;
      case 'budgetMax':
        setBudgetMax(value);
        break;
      default:
        setEstate(value);
        break;
    }
  }

  return (
    <div className='searchBox-container'>
      <nav>
        <div className="nav nav-tabs" id="nav-tab" role="tablist">
          <a onClick={() => _onClickChangeType('buy')} className="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Buy</a>
          <a onClick={() => _onClickChangeType('rent')} className="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Rent</a>
        </div>
      </nav>
      <div className="tab-content" id="nav-tabContent">
        <div className="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
          <div className="input-group">
            <div className="input-group-prepend">
              <img className="input-group-text icon-search" src="https://img.icons8.com/pastel-glyph/2x/search--v2.png" alt="search" />
            </div>
            <input value={search} onChange={_onChangeSearch} type="text" className="form-control search-input" placeholder="Search by district, estate, building, reference no." />
            <div onClick={_onClickSearch} className="input-group-append">
              <button className="input-group-text">Search</button>
            </div>
          </div>
          <RenderFilter
            budgetMinArray={budgetMinArray}
            budgetMaxArray={budgetMaxArray}
            budgetMin={budgetMin}
            budgetMax={budgetMax}
            district={district}
            estate={estate}
            onClickFilter={onClickFilter}
          />
        </div>
        <div className="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
          <div className="input-group">
            <div className="input-group-prepend">
              <img className="input-group-text icon-search" src="https://img.icons8.com/pastel-glyph/2x/search--v2.png" alt="search" />
            </div>
            <input type="text" className="form-control search-input" placeholder="Search by district, estate, building, reference no." />
            <div onClick={_onClickSearch} className="input-group-append">
              <span className="input-group-text cursor">Search</span>
            </div>
          </div>
          <RenderFilter
            budgetMinArray={budgetMinArray}
            budgetMaxArray={budgetMaxArray}
            budgetMin={budgetMin}
            budgetMax={budgetMax}
            district={district}
            estate={estate}
            onClickFilter={onClickFilter}
          />
        </div>
      </div>

    </div>
  )
}

const RenderFilter = ({ budgetMinArray, budgetMaxArray, budgetMin, budgetMax, estate, district, onClickFilter }) => {
  const districtArray = ['Eastern', 'Southern', 'Central and Western', 'Sham Shui Po', 'Kowloon City', 'Wong Tai Sin', 'Yau Tsim Mong', 'North', 'Kwun Tong'];
  const estateArray = ['Kornhill', 'Greenwood Terrace (Cw)', 'Taikoo Shing', 'Grand Promenade', 'Lei King Wan', 'Amoy Gardens', 'Laguna City', 'Telford Gardens', 'Sceneway Garden', 'Tak Bo Garden'];

  return (
    <div className="d-flex mt-2">
      <div className="dropdown">
        <button className="btn btn-secondary dropdown-toggle" type="button" id="district" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {district}
        </button>
        <div className="dropdown-menu" aria-labelledby="district">
          {
            districtArray.map((d, i) => <button key={i} onClick={() => onClickFilter('district', d)} className="dropdown-item">{d}</button>)
          }
        </div>
      </div>
      <div className="dropdown">
        <button className="btn btn-secondary dropdown-toggle ml-2" type="button" id="estate" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {estate}
        </button>
        <div className="dropdown-menu" aria-labelledby="estate">
          {
            estateArray.map((e, i) => <button key={i} onClick={() => onClickFilter('estate', e)} className="dropdown-item">{e}</button>)
          }
        </div>
      </div>
      <div className="dropdown">
        <button className="btn btn-secondary dropdown-toggle ml-2" type="button" id="budget" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {budgetMin}
        </button>
        <div className="dropdown-menu" aria-labelledby="budget">
          {
            budgetMinArray.map((b, i) => <button key={i} onClick={() => onClickFilter('budgetMin', b)} className="dropdown-item">{b}</button>)
          }
        </div>
      </div>
      <div className="dropdown">
        <button className="btn btn-secondary dropdown-toggle ml-2" type="button" id="budget" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          {budgetMax}
        </button>
        <div className="dropdown-menu" aria-labelledby="budget">
          {
            budgetMaxArray.map((b, i) => <button key={i} onClick={() => onClickFilter('budgetMax', b)} className="dropdown-item">{b}</button>)
          }
        </div>
      </div>
    </div>
  )
}

export default SearchBox;
