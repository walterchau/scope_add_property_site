import React, { useState, useEffect } from 'react';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip } from 'recharts';

// components
import Nav from './Nav';
// import Loading from './Loading';

const Dashboard = ({ firebase }) => {
  const [content, setContent] = useState('dashboard');

  const _onClickChange = to => {
    // console.log('_onClickChange', to);
    setContent(to);
  }

  const _renderContent = () => {
    // console.log('_renderContent', content);
    if (content === 'dashboard') {
      return (
        <div className="p-4">
          <h1>Dashboard</h1>
          <Chart />
        </div>
      )
    }

    return (
      <Records title={content} />
    )
  }

  return (
    <div className="">
      <div className="bg-info" style={{ height: '60px' }}>
        <Nav firebase={firebase} />
      </div>
      <div className="row">
        <nav className="col-md-2 d-none d-md-block bg-light sidebar">
          <div className="sidebar-sticky">
            <ul className="nav flex-column">
            {
              dashNav.map((d, i) => {
                return (
                  <li key={i} className="nav-item mt-3">
                    <span onClick={_ => _onClickChange(d.to)} className="nav-link active" style={{ cursor: 'pointer' }}>
                      {
                        d.name === 'Dashboard'
                          ? <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-home"><path d="M3 9l9-7 9 7v11a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2z"></path><polyline points="9 22 9 12 15 12 15 22"></polyline></svg>
                          : <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-bar-chart-2"><line x1="18" y1="20" x2="18" y2="10"></line><line x1="12" y1="20" x2="12" y2="4"></line><line x1="6" y1="20" x2="6" y2="14"></line></svg>
                          // : <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" strokeWidth="2" strokeLinecap="round" strokeLinejoin="round" className="feather feather-file"><path d="M13 2H6a2 2 0 0 0-2 2v16a2 2 0 0 0 2 2h12a2 2 0 0 0 2-2V9z"></path><polyline points="13 2 13 9 20 9"></polyline></svg>
                      }
                      {d.name}
                    </span>
                  </li>
                )
              })
            }
            </ul>
          </div>
        </nav>
        {
          _renderContent()
        }
      </div>
    </div>
  )
}

const Records = ({ title }) => {
  const [titleName, setTitleName] = useState(title);
  const [tableName, setTableName] = useState([]);
  const [tableContent, setTableContent] = useState([]);

  useEffect(() => {
    _initFetch();
  }, [title])

  const _initFetch = (t = title) => {
    console.log('title', title);
    setTitleName(title);
    let fetchUrl;
    switch (title) {
      case 'branch':
        fetchUrl = 'fetch_all_branches'
        break;
      case 'agents':
        fetchUrl = 'fetch_all_agents'
        break;
      case 'transactions':
        fetchUrl = 'fetch_all_transactions'
        break;
      case 'properties':
        fetchUrl = 'fetch_all_properties'
        break;
      default:
        fetchUrl = 'fetch_all_users'
    }
    fetch(`http://localhost:5000/${fetchUrl}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
    })
      .then(res => res.json())
      .then(doc => {
        _renderTableName(doc);
      })
      .catch(err => console.log(err));
  }

  const _onClickViewDetail = id => {
    console.log('_onClickViewDetail', id.slice(0,2));
    if (id.slice(0,2) === 'PR') {
      fetch(`http://localhost:5000/fetch_owner?property_id=${id}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        },
      })
        .then(res => res.json())
        .then(doc => {
          // console.log('doc', doc);
          setTitleName('Owner');
          _renderTableName(doc);
        })
        .catch(err => console.log(err));
    }

    if (id.slice(0,2) === 'BR') {
      window.open(`http://localhost:5000/generate_transaction_pdf?title=${id}_transaction.pdf&id=${id}`,'_blank');
    }
  }

  const _renderTableName = (list) => {
    setTableName([]);
    setTableContent([]);
    let _tableName = [];
    let _tableContent = [];
    for (let key in list[0]) {
      if (list[0].hasOwnProperty(key)) {
        // console.log('key', key);
        _tableName.push(<th key={key} scope="col">{key}</th>)
      }
    }
    for (let ele of list) {
      for (let key in ele) {
        _tableContent.push(<td onClick={_ => _onClickViewDetail(ele[key])} key={Math.random()} style={{ cursor: 'pointer' }}>{ele[key]}</td>)
      }
      _tableContent.push(<tr key={Math.random()}></tr>)
    }
    setTableName(_tableName);
    setTableContent(_tableContent);
  }

  return (
    <div className="col-md-8 d-none d-md-block p-4">
      {
        titleName === 'Owner'
          ? <span style={{ cursor: 'pointer' }} onClick={_ => _initFetch('properties')}>{'< Back'}</span>
          : undefined
      }
      <h1 className="text-capitalize">{titleName}</h1>
      <div className="bg-light">
        <table className="table">
          <thead className="thead-dark">
            <tr>
              { tableName }
            </tr>
          </thead>
          <tbody>
            { tableContent }
          </tbody>
        </table>
      </div>
    </div>
  )
}

const Chart = () => {
  const data = [{name: 'Page A', uv: 400, pv: 2400, amt: 2400}, {name: 'Page B', uv: 500, pv: 2500, amt: 2500}, {name: 'Page C', uv: 600, pv: 2600, amt: 2600}];
  return (
    <LineChart width={600} height={300} data={data} margin={{ top: 5, right: 20, bottom: 5, left: 0 }}>
      <Line type="monotone" dataKey="uv" stroke="#8884d8" />
      <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
      <XAxis dataKey="name" />
      <YAxis />
      <Tooltip />
    </LineChart>
  );
}

const dashNav = [
  {
    name: 'Dashboard',
    to: 'dashboard',
  },
  {
    name: 'Properties',
    to: 'properties',
  },
  {
    name: 'Customer',
    to: 'customer',
  },
  {
    name: 'Branch',
    to: 'branch',
  },
  {
    name: 'Agents',
    to: 'agents',
  },
  {
    name: 'Transactions',
    to: 'transactions',
  },
]


export default Dashboard;
