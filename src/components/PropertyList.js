import React, { useState, useEffect } from 'react';

// components
import { PropertyCard } from './Card';

const PropertyList = ({ properties, propertyType }) => {
  return (
    <div className="property-container">
      <Properties properties={properties} propertyType={propertyType} />
    </div>
  )
}

const Properties = ({ properties, propertyType }) => {
  return (
    <div className="mt-3">
      <h2>Properties List</h2>
      <div className="content-container">
      {
        properties.map((p, i) => {
          return (
            <PropertyCard
              key={i}
              property={p}
              propertyType={propertyType}
            />
          )
        })
      }
      </div>
    </div>
  )
}

export default PropertyList;
