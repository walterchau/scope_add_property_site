import React from 'react';

export const Card = ({ imgUrl, title, description }) => {
  return (
    <div className="card m-3 card-container">
      <img src={imgUrl} className="card-img-top card-img" alt="imgUrl" />
      <div className="card-body">
        <h5 className="card-title">{title}</h5>
        <p className="card-text">{description}</p>
      </div>
    </div>
  )
}

export const CardOverLay = ({ imgUrl, title, description, price }) => {
  return (
    <div className="card bg-dark text-white m-3 card-container">
      <img src={imgUrl} className="card-img" alt={imgUrl} />
      <div className="card-img-overlay overlay">
        <h5 className="card-title">{title}</h5>
        <p className="card-text">{description}</p>
        <h3 className="card-text">{price}</h3>
      </div>
    </div>
  )
}

export const PropertyCard = ({ property, propertyType }) => {
  return (
    <div className="card m-3 property-card-container">
      <img src={property.imgUrl || 'https://img.rea-asia.com/hk-squarefoot/premium/750x420-crop/olh86543d442-f71b-4d83-9a12-e934ca531b7e_1600x1200.jpeg'} className="card-img-top card-img" alt="imgUrl" />
      <div className="card-body">
        <h2>
        {
          propertyType === 'buy'
            ? 'Selling Price: HKD$' + property.selling_price
            : 'Rental Price: HKD$' + property.rental_price
        }
        </h2>
        <h5>District Name: {property.district_name}</h5>
        <h5>Estate Name: {property.estate_name}</h5>
        <h6>Floor: {property.floor_number}, Block: {property.block_number}</h6>
        <h6>Gross Floor Area: {property.gross_floor_area}</h6>
        <h6>Number of Bedrooms: {property.number_of_bedrooms}</h6>
        <h6>Carpark Provided: {property.car_park_provided ? "YES" : "NO"}</h6>
      </div>
    </div>
  )
}
