import React, { useEffect, useState } from 'react';
import { Link, Redirect } from "react-router-dom";

const Nav = ({ firebase }) => {
  const [redirect, setRedirect] = useState(null);
  const [user, setUser] = useState(null);

  useEffect(() => {
    _checkLogin();
  }, [])

  const _checkLogin = async () => {
    await firebase
      .auth()
      .onAuthStateChanged(auth => {
        // console.log('auth', auth);
        if (auth) {
          fetch(`http://localhost:5000/fetch_user?id=${auth.uid}`, {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json'
            },
          })
            .then(res => res.json())
            .then(doc => {
              // console.log(doc);
              setUser(doc[0]);
            })
            .catch(err => console.log(err));
        }
    });
  };

  const _onClickLogout = async () => {
    await firebase
      .auth()
      .signOut()
      .then(_ => {
        setUser(null);
        return setRedirect('/');
      }, error => {
        console.error('Sign Out Error', error);
      });
  }
  if (redirect) {
    return <Redirect to="/" />
  }
  if (user) {
    return (
      <div className="container">
        <Link to="/" className="nav-link nav-text nav-title">
          <h2 className="">K's Property</h2>
        </Link>
        <ul className="nav nav-container mt-2" style={{ 'zIndex': 10 }}>
          <li className="nav-item">
            <p className="nav-link active nav-text">Hi {user.first_name}</p>
          </li>
          <li className="nav-item">
            <Link
              to={{
                pathname: '/profile',
                state: {
                  name: user.first_name + ' ' + user.last_name,
                  budget_for_buying: user.budget_for_buying,
                  budget_for_rental: user.budget_for_rental,
                  email: user.email,
                  phone_number: user.phone_number,
                  preferred_district: user.preferred_district,
                  preferred_estate: user.preferred_estate,
                  status: user.status,
                }
              }}
              className="nav-link nav-text">Profile</Link>
          </li>
          {
            user.first_name === 'peter'
              ? <li className="nav-item">
                  <Link
                    to={{pathname: '/dashboard',}}
                    className="nav-link nav-text">Dashboard</Link>
                </li>
              : undefined
          }
          <li onClick={_onClickLogout} className="nav-item" style={{ cursor: 'pointer' }}>
            <p className="nav-link active nav-text">Logout</p>
          </li>
        </ul>
      </div>
    )
  }
  return (
    <ul className="nav nav-container" style={{ 'zIndex': 10 }}>
      <li className="nav-item">
        <Link to="/login" className="nav-link active nav-text">Login</Link>
      </li>
      <li className="nav-item">
        <Link to="/signup" className="nav-link nav-text">Signup</Link>
      </li>
      <li className="nav-item">
        <Link to="/postListing" className="nav-link nav-text">Post Listing</Link>
      </li>
    </ul>
  )
}

export default Nav;
