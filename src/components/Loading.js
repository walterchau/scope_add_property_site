import React from 'react';

const Loading = () => {
  return (
    <div className="container d-flex justify-content-center align-items-center p-5">
      <img className="" alt="loading" src="/loading.gif" />
    </div>
  )
}

export default Loading;
