import React, { useState, useRef } from 'react';
import { useLocation } from 'react-router-dom';

// components
import Nav from './Nav';
import PropertyList from './PropertyList';

const Profile = ({ firebase }) => {
  const location = useLocation();
  const propertyRef = useRef(null);
  const [propertyType, setPropertyType] = useState('');
  const [propertyList, setPropertyList] = useState([]);
  const [togglePropertyList, setTogglePropertyList] = useState(false);

  const { name, email, phone_number, budget_for_buying, budget_for_rental, preferred_district, preferred_estate } = location.state;

  const _onClickSearch = async type => {
    // console.log('_onClickSearch', name, type);
    let query = `estate_name=${preferred_estate}&district_name=${preferred_district}`;
    query = type === 'buy'
      ? query + `&selling_price_min=0&selling_price_max=${budget_for_buying}`
      : query + `&rental_price_min=0&rental_price_max=${budget_for_rental}`

    setPropertyType(type);
    const fetchUrl = `fetch_property?${query}`
    fetch(`http://localhost:5000/${fetchUrl}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
    })
      .then(res => res.json())
      .then(doc => {
        setPropertyList(doc);
        setTogglePropertyList(true);
        propertyRef.current.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
      })
      .catch(err => console.log(err));
  }

  return (
    <div>
      <div className="bg-info" style={{ height: '60px' }}>
        <Nav firebase={firebase} />
      </div>
      <section className="d-flex p-2 mt-4">
        <div className="container-fluid">
          <div className="row">
            <div className="col-lg-5">
              <h1 style={{ fontSize: '5.5rem' }}>Profile</h1>
              <div className="hero-text mt-3">
                <h2>{name}</h2>
                <p>I’m a digital designer in love with photography, painting and discovering new worlds and cultures.</p>
              </div>
              <div className="hero-info">
                <h2>General Info</h2>
                <ul>
                  <li><span>E-mail: </span>{email}</li>
                  <li><span>Phone: </span>{phone_number || 'Null'}</li>
                  <li><span>Budget for buying: </span>HKD$ {budget_for_buying}</li>
                  <li><span>Budget for rental: </span>HKD$ {budget_for_rental}</li>
                  <li><span>Preferred district: </span>{preferred_district}</li>
                  <li><span>Preferred estate: </span>{preferred_estate}</li>
                </ul>
              </div>
              <button onClick={_ => _onClickSearch('buy')} type="button" className="btn btn-info mt-2">Search Based on the preferences and budget for Sale</button>
              <button onClick={_ => _onClickSearch('rent')} type="button" className="btn btn-info mt-2">Search Based on the preferences and budget for Rent</button>
            </div>
            <div className="col-lg-5">
              <figure className="hero-image">
                <img src="https://colorlib.com/preview/theme/civic/img/hero.jpg" alt="5" />
              </figure>
            </div>
            {
              togglePropertyList
                ? <div ref={propertyRef}>
                    <PropertyList properties={propertyList} propertyType={propertyType} />
                  </div>
                : undefined
            }
          </div>
        </div>
      </section>
    </div>
  )
}

export default Profile;
