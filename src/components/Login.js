import React, { useState, useEffect } from 'react';
import { Link } from "react-router-dom";
import { Redirect, useLocation } from 'react-router-dom';

// components
import Loading from './Loading';

const Login = ({ firebase }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [redirect, setRedirect] = useState(null);
  const location = useLocation();

  useEffect(() => {
    // console.log(location);
  })

  const _onClickLogin = () => {
    // console.log('_onClickLogin', email, password);
    setIsLoading(true);

    firebase
      .auth()
      .signInWithEmailAndPassword(email, password)
      .then(() => {
        setIsLoading(false);
        location.state && location.state.type === 'dashboard'
          ? setRedirect('/dashboard')
          : setRedirect('/');
      })
      .catch(error => {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        console.log(errorCode, errorMessage);
      });
  }

  if (isLoading) {
    return <Loading />
  }

  if (redirect) {
    return <Redirect to={redirect} />
  }

  return (
    <div className="d-flex justify-content-center login-container">
      <div className="form-signin login-form p-4 w-50">
        <h1 className="h1 mb-3 font-weight-normal">
        {
          location.state && location.state.type === 'dashboard'
            ? "Agent Login"
            : "Customer Login"
        }
        </h1>
        <h1 className="h3 mb-3 font-weight-normal">Please sign in</h1>
        <label htmlFor="inputEmail" className="sr-only">Email address</label>
        <input value={email} onChange={e => setEmail(e.target.value)} type="email" id="inputEmail" className="form-control" placeholder="Email address" required="" autoFocus="" />
        <label htmlFor="inputPassword" className="sr-only">Password</label>
        <input value={password} onChange={e => setPassword(e.target.value)} type="password" id="inputPassword" className="form-control" placeholder="Password" required="" />
        <div className="checkbox mb-3">
          <label>
            <input type="checkbox" value="remember-me" /> Remember me
          </label>
        </div>
        <button onClick={_onClickLogin} className="btn btn-lg btn-primary btn-block mb-3">Sign in</button>
        <Link to="/signup" className="mt-2 mb-3 text-muted">Don't have account yet?</Link>
        <p className="mt-4 mb-3 text-muted">© 2019</p>
      </div>
    </div>
  )
}

// const AgentInput = ({ agentType, onClickFilter }) => {
//   const agentTypeArray = ['Agent', 'Manager'];
//
//   return (
//     <div className="d-flex mt-3 mb-2">
//       <div className="dropdown">
//         <button className="btn btn-secondary dropdown-toggle" type="button" id="agentType" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
//           {agentType ? agentType : 'Agent Type'}
//         </button>
//         <div className="dropdown-menu" aria-labelledby="agentType">
//           {
//             agentTypeArray.map((d, i) => <button key={i} onClick={() => onClickFilter('agent', d)} className="dropdown-item">{d}</button>)
//           }
//         </div>
//       </div>
//     </div>
//   )
// }

export default Login;
