import React, { useState, useRef } from 'react';
import { BrowserRouter as Router, Route } from "react-router-dom";

// components
import ErrorBoundary from './components/ErrorBoundary';
import Header from './components/Header';
import SearchBox from './components/SearchBox';
import Nav from './components/Nav';
import Login from './components/Login';
import Signup from './components/Signup';
import PropertyList from './components/PropertyList';
import Dashboard from './components/Dashboard';
import Profile from './components/Profile';
import Footer from './components/Footer';
import { Card, CardOverLay } from './components/Card';

import './App.css';


import * as firebase from "firebase/app";
// Add the Firebase services that you want to use
import "firebase/auth";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyD6z6l2H1DW7HX4Xk2HVFqDfzxj5Dhs58c",
  authDomain: "scope-istore.firebaseapp.com",
  databaseURL: "https://scope-istore.firebaseio.com",
  projectId: "scope-istore",
  storageBucket: "",
  messagingSenderId: "781183777718",
  appId: "1:781183777718:web:7f769d1706bd6666"
};

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

function App() {
  return (
    <ErrorBoundary>
      <Router>
        <Route path="/" exact render={() => <Home /> } />
        <Route path="/Login" render={() => <Login firebase={firebase} />} />
        <Route path="/Signup" render={() => <Signup firebase={firebase} />} />
        <Route path="/Dashboard" exact render={() => <Dashboard firebase={firebase} />} />
        <Route path="/Profile" exact render={() => <Profile firebase={firebase} />} />
      </Router>
      <Footer />
    </ErrorBoundary>
  );
}

export default App;

const Home = () => {
  const propertyRef = useRef(null);
  const [togglePropertyList, setTogglePropertyList] = useState(false);
  const [propertyList, setPropertyList] = useState([]);
  const [propertyType, setPropertyType] = useState('');

  const onClickSearch = async (estate, district, type, budgetMin, budgetMax) => {
    // console.log('onClickSearch', estate, type, typeof(budgetMax));
    setPropertyType(type);
    let query = `estate_name=${estate}&district_name=${district}`;
    query = type === 'buy'
      ? query + `&selling_price_min=0`
      : query + `&rental_price_min=0`

    const fetchUrl = `fetch_property?${query}`
    fetch(`http://localhost:5000/${fetchUrl}`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json'
      },
    })
      .then(res => res.json())
      .then(doc => {
        setPropertyList(doc);
        setTogglePropertyList(true);
        propertyRef.current.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});
      })
      .catch(err => console.log(err));
  }

  return (
    <div className="App">
      <Header />
      <Nav firebase={firebase} />
      <SearchBox onClickSearch={onClickSearch}/>
      {
        togglePropertyList
          ? <div ref={propertyRef}>
              <PropertyList properties={propertyList} propertyType={propertyType} />
            </div>
          : <div>
              <NewProperties />
              <HotProperties />
            </div>
      }
    </div>
  )
}

const NewProperties = () => {
  return (
    <div className="mt-3">
      <h2>New Properties</h2>
      <div className="content-container">
      {
        newProperties.map((p, i) => {
          return (
            <Card
              key={i}
              imgUrl={p.imgUrl}
              title={p.title}
              description={p.description}
            />
          )
        })
      }
      </div>
    </div>
  )
}

const HotProperties = () => {
  return (
    <div className="mt-3">
      <h2>Hot Properties</h2>
      <div className="content-container">
      {
        hotProperties.map((p, i) => {
          return (
            <CardOverLay
              key={i}
              imgUrl={p.imgUrl}
              title={p.title}
              description={p.description}
              price={p.price}
            />
          )
        })
      }
      </div>
    </div>
  )
}

const newProperties = [
  {
    title: 'Seaside Sonata',
    description: 'Urban Renewal Authority, Cheung Kong, Cheung Sha Wan, Kowloon',
    imgUrl: 'https://img.rea-asia.com/gohome-newlaunch/premium/250x250/GH_newproperty_banner/seasidesoneta_360x250.jpg'
  },
  {
    title: 'Emerald Bay',
    description: 'China Evergrande, Tuen Mun, New Territories',
    imgUrl: 'https://s3-hk.rea-asia.com/designteam/emeraldbay_360x250.jpg'
  },
  {
    title: 'Cullinan West III',
    description: 'Sham Shui Po, Kowloon, Sun Hung Kai Properties Limited, MTR',
    imgUrl: 'https://img.rea-asia.com/gohome-newlaunch/premium/250x250/GH_newproperty_banner/cullinanwest3_360x250.jpg'
  },
];

const hotProperties = [
  {
    title: 'Seaside Sonata',
    description: 'Urban Renewal Authority, Cheung Kong, Cheung Sha Wan, Kowloon',
    imgUrl: 'https://img.rea-asia.com/gohome-newlaunch/premium/250x250/GH_newproperty_banner/seasidesoneta_360x250.jpg',
    price: 'Rent: $20,500',
    type: 'rent',
    estate: 'Urban Renewal Authority',
    district: 'Cheung Kong, Cheung Sha Wan, Kowloon',
    grossArea: '340 ft sq',
  },
  {
    title: 'Emerald Bay',
    description: 'China Evergrande, Tuen Mun, New Territories',
    imgUrl: 'https://s3-hk.rea-asia.com/designteam/emeraldbay_360x250.jpg',
    price: 'Sale: $12,050,000',
    type: 'sale',
    estate: 'China Evergrande',
    district: 'Tuen Mun, New Territories',
    grossArea: '820 ft sq',
  },
  {
    title: 'Seaside Sonata',
    description: 'Urban Renewal Authority, Cheung Kong, Cheung Sha Wan, Kowloon',
    imgUrl: 'https://img.rea-asia.com/gohome-newlaunch/premium/250x250/GH_newproperty_banner/seasidesoneta_360x250.jpg',
    price: 'Rent: $12,000',
    type: 'rent',
    estate: 'Urban Renewal Authority',
    district: 'Cheung Kong, Cheung Sha Wan, Kowloon',
    grossArea: '410 ft sq',
  },
  {
    title: 'Cullinan West III',
    description: 'Sham Shui Po, Kowloon, Sun Hung Kai Properties Limited, MTR',
    imgUrl: 'https://img.rea-asia.com/gohome-newlaunch/premium/250x250/GH_newproperty_banner/cullinanwest3_360x250.jpg',
    price: 'Sale: $8,199,000',
    type: 'sale',
    estate: 'Sun Hung Kai Properties Limited, MTR',
    district: 'Sham Shui Po, Kowloon',
    grossArea: '530 ft sq',
  },
  {
    title: 'Emerald Bay',
    description: 'China Evergrande, Tuen Mun, New Territories',
    imgUrl: 'https://s3-hk.rea-asia.com/designteam/emeraldbay_360x250.jpg',
    price: 'Rent: $15,000',
    type: 'rent',
    estate: 'China Evergrande',
    district: 'Tuen Mun, New Territories',
    grossArea: '310 ft sq',
  },
  {
    title: 'Cullinan West III',
    description: 'Sham Shui Po, Kowloon, Sun Hung Kai Properties Limited, MTR',
    imgUrl: 'https://img.rea-asia.com/gohome-newlaunch/premium/250x250/GH_newproperty_banner/cullinanwest3_360x250.jpg',
    price: 'Rent: $9,000',
    type: 'rent',
    estate: 'Sun Hung Kai Properties Limited, MTR',
    district: 'Sham Shui Po, Kowloon',
    grossArea: '190 ft sq',
  },
];
