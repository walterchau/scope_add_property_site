const express = require('express');
const bodyParser = require('body-parser');
const mysql = require('mysql');
const moment = require('moment');
const fs = require("fs");
const app = express();

// const {getHomePage} = require('./routes/index');
// const {addPlayerPage, addPlayer, deletePlayer, editPlayer, editPlayerPage} = require('./routes/player');
const port = 5000;

// create connection to database
// the mysql.createConnection function takes in a configuration object which contains host, user, password and the database name.
const db = mysql.createConnection ({
    host: 'localhost',
    user: 'root',
    password: 'password',
    database: 'add_database'
});

// connect to database
db.connect((err) => {
    if (err) {
        throw err;
    }
    console.log('Connected to database');
});
global.db = db;

// configure middleware
app.set('port', process.env.port || port); // set express to use this port
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); // parse form data client
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "http://localhost:3000"); // update to match the domain you will make the request from
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// routes for the app
// fetch all properties
app.get('/fetch_all_properties', (req, res) => {

  let query = `SELECT * FROM properties`;

  // execute query
  db.query(query, (err, result) => {
    if (err) {
      console.log('err', err);
      return;
    }
    console.log('result', result);
    return res.send(result);
  });
})
// fetch property by name and type
app.get('/fetch_property', (req, res) => {
  const { estate_name, district_name, rental_price_min, rental_price_max, selling_price_min, selling_price_max } = req.query;
  console.log(rental_price_min, rental_price_max, selling_price_min, selling_price_max);

  let select = "d.district_name, e.estate_name, p.block_number, p.flat_number, p.flat_number, p.gross_floor_area, p.number_of_bedrooms, p.car_park_provided, p.allow_to_bargain"

  select = rental_price_min ? select + ', p.rental_price' : select + ', p.selling_price';

  console.log('select', select);

  let userQuery = '';
  userQuery = estate_name ? userQuery + `AND e.estate_name LIKE '${estate_name}' ` : userQuery;
  userQuery = district_name ? userQuery + `AND d .district_name LIKE '${district_name}' ` : userQuery;
  userQuery = rental_price_min ? userQuery + `AND ${rental_price_min} < p.rental_price ` : userQuery;
  userQuery = rental_price_max ? userQuery + `AND p.rental_price < ${rental_price_max} ` : userQuery;
  userQuery = selling_price_min ? userQuery + `AND ${selling_price_min} < p.selling_price ` : userQuery;
  userQuery = selling_price_max ? userQuery + `AND p.selling_price < ${selling_price_max} ` : userQuery;

  console.log('userQuery', userQuery);

  let query = `
SELECT
    ${select}
FROM
    estates e
        JOIN districts d ON d.id = e.district_id
        JOIN properties p ON e.id = p.estate_id
WHERE
    p.status = 'active'
    ${userQuery};`

  console.log('query', query);
  // let query = `SELECT * FROM properties WHERE estate_name = '${name}' and ${type}`;
  // execute query
  db.query(query, (err, result) => {
    if (err) {
      console.log('err', err);
      return;
    }
    console.log('result', result);
    return res.send(result);
  });
})

// fetch owner by property id
app.get('/fetch_owner', (req, res) => {
  const property_id = req.query.property_id;

  let query = `
SELECT
    o.*,
    e.estate_name,
    d.district_name
FROM
    owners o
        JOIN properties p ON p.owner_id = o.id
        JOIN estates e ON e.id = p.estate_id
        JOIN districts d ON d.id = e.district_id
WHERE
    p.Property_id = '${property_id}';`

  // execute query
  db.query(query, (err, result) => {
    if (err) {
      console.log('err', err);
      return;
    }
    console.log('result', result);
    return res.send(result);
  });
})

// add new user
app.post('/add_user', (req, res) => {
  console.log('req', req.body);
  const { id, preferred_district, preferred_estate, first_name, last_name, email, budget_for_buying, budget_for_rental } = req.body;
  const created_at = moment().format("YYYY-MM-DD");
  const last_modified_at = moment().format("YYYY-MM-DD");
  const status = 'active';

  let query = `INSERT INTO customers (id, preferred_district, preferred_estate, first_name, last_name, email, budget_for_buying, budget_for_rental, status, last_modified_at, created_at ) VALUES('${id}', '${preferred_district}', '${preferred_estate}', '${first_name}', '${last_name}', '${email}', ${budget_for_buying}, ${budget_for_rental}, '${status}', '${last_modified_at}', '${created_at}')`;

  // execute query
  db.query(query, (err, result) => {
    if (err) {
      console.log('err', err);
      return;
    }
    console.log('result', result);
    return res.send(result);
  });
})

// fetch all users
app.get('/fetch_all_users', (req, res) => {

  let query = `SELECT * FROM customers`;

  // execute query
  db.query(query, (err, result) => {
    if (err) {
      console.log('err', err);
      return;
    }
    console.log('result', result);
    return res.send(result);
  });
})

// fetch user by id
app.get('/fetch_user', (req, res) => {
  const id = req.query.id;

  let query = `SELECT * FROM customers WHERE id = '${id}'`;

  // execute query
  db.query(query, (err, result) => {
    if (err) {
      console.log('err', err);
      return;
    }
    console.log('result', result);
    return res.send(result);
  });
})

// fetch all branches
app.get('/fetch_all_branches', (req, res) => {

  let query = `SELECT * FROM branches`;

  // execute query
  db.query(query, (err, result) => {
    if (err) {
      console.log('err', err);
      return;
    }
    console.log('result', result);
    return res.send(result);
  });
})

// fetch all agents
app.get('/fetch_all_agents', (req, res) => {

  let query = `SELECT * FROM staff`;

  // execute query
  db.query(query, (err, result) => {
    if (err) {
      console.log('err', err);
      return;
    }
    console.log('result', result);
    return res.send(result);
  });
})

// fetch all transactions
app.get('/fetch_all_transactions', (req, res) => {

  let query = `SELECT * FROM transactions`;

  // execute query
  db.query(query, (err, result) => {
    if (err) {
      console.log('err', err);
      return;
    }
    console.log('result', result);
    return res.send(result);
  });
})

// fetch to generate pdf
app.get('/generate_transaction_pdf', (req, res) => {
  const title = req.query.title || 'Transactions.pdf';
  const id = req.query.id || 'BR00001';

  const PDFDocument = require('pdfkit');
  // Create a document
  const doc = new PDFDocument;

  // Pipe its output somewhere, like to a file or HTTP response
  // See below for browser usage
  doc.pipe(fs.createWriteStream('output.pdf'));

  res.setHeader('Content-Type', 'application/pdf');
  res.setHeader('Content-Disposition', `attachment; filename=${title}`);
  doc.pipe(res);

  const query = `
SELECT
    t.id AS 'Transaction ID',
    e.estate_name AS 'Estate',
    o.first_name AS 'Owner',
    s.first_name AS 'Staff',
    t.transaction_type AS 'Type',
    t.transaction_price AS 'Price',
    t.commission AS 'Commission',
    t.created_at AS 'Date'
FROM
    transactions t
        JOIN properties p ON p.Property_id = t.property_id
        JOIN estates e ON e.id = p.estate_id
        JOIN owners o ON o.id = t.owner_id
        JOIN staff s ON s.id = t.agent_id
        JOIN branches b ON b.id = s.branch_id
WHERE
    b.id = '${id}'
ORDER BY t.transaction_type;`

  // execute query
  db.query(query, (err, result) => {
    if (err) {
      console.log('err', err);
      return;
    }
    console.log('result', result);
    // Embed a font, set the font size, and render some text
    doc
      .fontSize(30)
      .text(`K's Property`, {
        width: 500,
        align: 'center'
      })
      .moveDown();

    doc
      .fontSize(25)
      .text(`Branch ${id} Transactions`, {
        width: 500,
        align: 'center'
      })
      .moveDown();

    let header = '';
    for (let key in result[0]) {
      if (result[0].hasOwnProperty(key)) {
        header += ` ${key} `
      }
    }
    doc.rect(doc.x, 10, 505, doc.y+4).stroke();

    doc
      .fontSize(15)
      .text(`${header} `, {
        width: 500,
        align: 'right'
      })
      .moveDown();

    doc.rect(doc.x, 10, 505, doc.y+4).stroke();

    for (ele of result) {
      let row = '';
      for (let key in ele) {
        if (ele.hasOwnProperty(key)) {
          if (key === 'Date') {
            row += ` ${moment().format("YYYY-MM-DD")} `
          } else if (key === 'Price' && ele[key] < 50000) {
            row += `     ${ele[key]}    `
          } else row += ` ${ele[key]} `
        }
      }
      doc
        .fontSize(12)
        .text(`${row} `, {
          width: 500,
          align: 'right'
        })
        .moveDown();
      doc.rect(doc.x, 10, 505, doc.y+4).stroke();
    }
    // Finalize PDF file
    doc.end();
  });
})


// set the app to listen on the port
app.listen(port, () => {
  console.log(`Server running on port: ${port}`);
});
